Listen! Listen! Listen!: Listening came first!
Revise! Revise! Revise!: Revise means "to read again".


Parts of Speech:

Verb: action or state
words: (to) be, is, have, do, like, work, sing, can, must, stop, love
sentences: EnglishClub is a website. I <like> EnglishClub.

Verbs may be treated as two different parts of speech:
* lexical verbs (work, like, run)
* auxiliary verbs (be, have, must)

Noun: thing or person
words: pen, dog, work, music, town, London, teacher, John, English
sentences: This is my <dog>. He lives in my <house>. We live in <London>.

Adjective: describes a noun
words: good, big, red, well, interesting
sentences: My dogs are <big>. I like <big> dogs.

Determiner: limits or 'determines' a noun
words: a/an, the, 2, some, many
sentences: I have <two> dogs and <some> rabbits.

Note: Determiners may be treated as adjectives, instead of being a separate part
of speech.

Adverb: describes a verb, adjective or adverb
words: quickly, silently, well, badly, very, really
sentences: My dog eats <quickly>. When he is <very> hungry, he eats <really> quickly.

Pronoun: replaces a noun
words: I, you, he, she, some
sentences: Tara is Indian. <She> is beautiful.

* definite pronoun: he, she, their, it, that

* indefinite pronoun: refers to a general, vague, or unknown person, object,
  group, or amount. Most of the time, the referred-to object is identified
  elsewhere in the sentence or doesn't need to be precisely identified.

  * I've had one piece of pie, and I'd like <another>.
  * <Much> have been written about the way Americans feel about their cars.

  May of the most common indefinite pronouns are formed by combining the
  prefixes every-, any-, some-, and no- with the roots -thing, -one, and
  -body. Any and some can also stand alone as indefinite pronouns.

  * "I am a nice person." Claire insisted. "Ask <anyone>."
  * It's not true that the American moon landing was staged in a television
    studio, as <some> believe.

Preposition: links a noun to another word
words: in, to, at, after, on, but
sentences: We went <to> school <on> Monday.

Conjunction: joins clauses or sentences or words
words: and, but, when
sentences: I like dogs <and> I like cats. I like cats <and> dogs. I like dogs
           <but> I don't like cats.

Interjection: short exclamation, sometimes inserted into a sentence
words: oh!, ouch!, hi!, well
sentences: <Ouch!> That hurts! <Hi!> How are you? <Well>, I don't know.


word        part of speech              example
------------------------------------------------------------------------

work        noun                        My work is easy.
            verb                        I work in London.

but         conjunction                 John came but Mary didn't come.
            preposition                 Everyone came but Mary.

well        adjective                   Are you well?
            adverb                      She speaks well.
            interjection                Well! That's expensive!

afternoon   noun                        We ate in the afternoon.
            noun acting as adjective    We had afternoon tea.
