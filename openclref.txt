Compiling an OpenCL program using a CL/cl.h file:
gusdt:~/codelearn/opencl$ gcc -o program program.c -lOpenCL
program.c:2:10: fatal error: CL/cl.h: No such file or directory
    2 | #include <CL/cl.h>
      |          ^~~~~~~~~
compilation terminated.
gusdt:~/codelearn/opencl$

$ doas apt-get install opencl-headers
$ doas apt-get install ocl-icd-libopencl1
$ doas apt-get install ocl-icd-opencl-dev


/usr/bin/ld: cannot find -lOpenCL:
gusdt:~/codelearn/opencl$ gcc -o program program.c -lOpenCL
In file included from /usr/include/CL/cl.h:20,
                 from program.c:2:
/usr/include/CL/cl_version.h:22:9: note: ‘#pragma message: cl_version.h: CL_TARGET_OPENCL_VERSION is not defined. Defaulting to 300 (OpenCL 3.0)’
   22 | #pragma message("cl_version.h: CL_TARGET_OPENCL_VERSION is not defined. Defaulting to 300 (OpenCL 3.0)")
      |         ^~~~~~~
/usr/bin/ld: cannot find -lOpenCL: No such file or directory
collect2: error: ld returned 1 exit status
gusdt:~/codelearn/opencl$

The solution there was to make a link for the library to a known lib location,
assuming that OpenCL library located in /usr/lib/x86_64-linux-gnu/:
$ doas ln -s /usr/lib/x86_64-linux-gnu/libOpenCL.so.1 /usr/lib/libOpenCL.so
Another option, you can also add the library folder to the Libraries path:
$ export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/lib/x86_64-linux-gnu/"


No OpenCL platform detected:
$ doas apt-get install mesa-opencl-icd


Compiling OpenCL source code:
$ gcc -Wall -Wextra -D CL_TARGET_OPENCL_VERSION=100 program.c -o program -lOpenCL
 * -Wall -Wextra turns on all warnings (highest sensible level)
 * -D instructs the preprocessor to create a define with NAME:VALUE
        CL_TARGET_OPENCL_VERSION enables/disables API functions corresponding to the
        defined version. Setting it to 100 will disable all API functions in the header
        that are newer than OpenCL 1.0
 * -I sets additional paths to the include directory search paths
 * program.c is the name of the input source file
 * -o sets the name of the output executable (default would be a.out)
 * -l instructs linker to link library OpenCL



OpenCL, a framework for developing applications optimized for parallel computing.
GPU drivers implement support for this framework, and the application's performance
depends on them.

$ clinfo

$ clinfo --list

$ clinfo | grep Number

In OpenCL terminology, "platform" refers to the type of availabel computing
devices. This can be a CPU, GPU, or even an FPGA.

If the "Number of platforms" column shows 0, it indicates that the operating system
isn't ready to run OpenCL applications. In this case, check if the appropriate drivers
are installed on the system. If issues persist, try completely removing the GPU driver,
rebooting the server, and then reinstalling them.

$ clpeak




Reference:
Programming GPUs with Python: PyOpenCL and PyCUDA
http://homepages.math.uic.edu/~jan/mcs572f16/mcs572notes/lec29.html

Getting started with OpenCL using mesa/rusticl
https://nullr0ute.com/2023/12/getting-started-with-opencl-using-mesa-rusticl/

Installing the latest GPGPU software packages on Ubuntu 20.04:
https://community.intel.com/t5/GPU-Compute-Software/Installing-the-latest-GPGPU-software-packages-on-Ubuntu-20-04/m-p/1299807

Intel OpenCL Intel CPU not detected:
https://community.intel.com/t5/OpenCL-for-CPU/Intel-OpenCL-Intel-CPU-not-detected/m-p/1383450
