plt.plot(range(10), linestyle='--', marker='o', color='b')
A short form for the above: plt.plot (range(10), '--bo')

List of line and marker styles:
-		solid line style
--		dashed line style
-.		dash-dot line style
:		dotted line style
.		point marker
,		pixel marker
o		circle marker
v		triangle_down marker
^		triangle_up marker
<		triangle_left marker
>		triangle right marker
1		tri_down marker
2		tri_up marker
3		tri_left marker
4		tri_right marker
s		square marker
p		pentagon marker
*		star marker
h		hexagon1 marker
H		hexagon2 marker
+		plus marker
x		x marker
D		diamond marker
d		thin_diamond marker
|		vline marker
_		hline marker


# Text rendering with LaTeX
plt.title('alpha > beta')      # plain text
plt.title(r'$\alpha > \beta$') # math text

Reference:
Named Colors: https://matplotlib.org/3.1.0/gallery/color/named_colors.html
Line Bars and Markers: https://matplotlib.org/3.1.3/gallery/lines_bars_and_markders/
Style Sheets: https://matplotlib.org/3.2.1/gallery/style_sheets/style_sheets_reference.html
Text rendering with LaTeX: https://matplotlib.org/tutorials/text/usetex.html#sphx-glr-tutorials-text-usetex-py
