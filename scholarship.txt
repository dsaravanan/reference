# ONGC Scholarship to Meritorious SC/ST Students
ONGC is inviting SC/ST students, who wish to pursue professional courses for higher education, for availing of scholarship assistance. The scholarship is aimed at supporting students 
from marginalised sections to get higher education opportunities. Eligibility: SC/ST students enrolled in the first year of graduate engineering, MBBS, master's in geology/geophysics,
or MBA programmes, may apply, if they have scored more than 60% marks in class XII/final-year graduation exams. Applicants must not be above 30 years of age and must have an annual
family income less than or equal to Rs.4.50 lakhs. Prizes and Rewards: Selected students will receive Rs.48,000 each per annum for completing their graducation/postgraduation. 
Application: Offline Deadline: October 15 http://www.b4s.in/plus/OST2

# Pre-Matric Scholarship for Students with Disabilities
Department of Empowerment of Persons with Disabilities, Government of India, is providing financial assistance to physically-challenged students for pursuing secondary-level education.
Under this programme, students with disabilities will receive scholarships to manage academic expenses and prepare for their career aspirations. Eligibility: Students with more than 40% 
disability condition, currently studying in class IX or X may apply. Only students with annual family income below Rs.2.50 lakhs are eligible. Prize and Rewards: Eligible students may 
receive maintenance allowance up to Rs.800/month, book grant of Rs.1,000/year and disability allowance between Rs.2,000/year to Rs.4,000/year. Application: Online Deadline: October 15
http://www.b4s.in/plus/PSF1

# National Means-Cum-Merit Scholarship Scheme
Students who are facing financial difficulties in continuning their education after class VIII are invited to apply for this programme, which provides scholarship assistance to manage
academic expenses. Department of School Education and Literacy, Government of India, is providing these national-level scholarships to encourage students across the nation to continue
their secondary education. Eligibility: Students currently enrolled in class IX, with at least 55% marks in class VIII (relaxable by 5% for SC/ST students) and at least 55% in class VIII
(mandatory for all categories) may apply, if their annual family income is less than Rs.1.50 lakhs. Prizes and Rewards: Selected students will receive a scholarship amount of Rs.12,000 
per month distributed in equal monthly instalments. Application: Online Deadline: October 15 http://www.b4s.in/plus/NMC6

Courtesy: www.buddy4study.com
