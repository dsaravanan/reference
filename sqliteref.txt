sqlite3  # at command prompt will provide SQLite command prompt
.exit/.quit    # exit SQLite prompt
.databases     # list names and files of attached databases
.show          # show the current values for various settings
.stat ON|OFF   # turn stats on or off
.help          # show this message
