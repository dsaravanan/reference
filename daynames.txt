DAYS OF THE WEEK
(origins pagan gods/pre-christian)

Sunday      Sun's Day       (Sonntag)
Monday      Moon's Day      (Montag, Lundi)
Tuesday     Tiu's Day       (god of war/the sky) (Mars ... Mardi)
Wednesday   Woden's Day     (chief god) (Mercury ... Mercredi)
Thursday    Thor's Day      (god of thunder ... Donnerstag) (Jove ...Jeudi)
Friday      Freya's Day     (god of love ... Freitag) (Venus ... Vendredi)
Saturday    Saturn's Day    (Roman god of agriculture) (Saturn)
