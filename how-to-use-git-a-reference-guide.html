<!DOCTYPE html>
<html lang="en">

      </button> How To Use Git: A Reference Guide
    </h1>

    <span class="meta-section timestamp"><span class="tutorial-date-text"></span><span class="tutorial-date">October 10, 2018</span></span>

          <div class="author-and-date">
            <div class="tutorial-author">
              Lisa Tagliaferri
            </div>
          </div>

    <div class="content-body tutorial-content" data-growable-markdown>
      <h2 id="git-cheat-sheet">Git Cheat Sheet</h2>

<h3 id="introduction">Introduction</h3>

<p>Teams of developers and open-source software maintainers typically manage their projects through Git, a distributed version control system that supports collaboration. </p>

<p>This cheat sheet-style guide provides a quick reference to commands that are useful for working and collaborating in a Git repository. To install and configure Git, be sure to read “<a href="https://www.digitalocean.com/community/tutorials/how-to-contribute-to-open-source-getting-started-with-git">How To Contribute to Open Source: Getting Started with Git</a>.”</p>

<p><strong>How to Use This Guide:</strong></p>

<ul>
<li>This guide is in cheat sheet format with self-contained command-line snippets.</li>
<li>Jump to any section that is relevant to the task you are trying to complete.</li>
<li>When you see <code><span class="highlight">highlighted text</span></code> in this guide’s commands, keep in mind that this text should refer to the commits and files in <em>your own</em> repository.</li>
</ul>

<h2 id="set-up-and-initialization">Set Up and Initialization</h2>

<p>Check your Git version with the following command, which will also confirm that Git is installed.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git --version
</li></ul></code></pre>
<p>You can initialize your current working directory as a Git repository with <code>init</code>.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git init
</li></ul></code></pre>
<p>To copy an existing Git repository hosted remotely, you’ll use <code>git clone</code> with the repo’s URL or server location (in the latter case you will use <code>ssh</code>).</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git clone <span class="highlight">https://www.github.com/username/repo-name</span>
</li></ul></code></pre>
<p>Show your current Git directory’s remote repository.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git remote
</li></ul></code></pre>
<p>For a more verbose output, use the <code>-v</code> flag.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git remote -v
</li></ul></code></pre>
<p>Add the Git upstream, which can be a URL or can be hosted on a server (in the latter case, connect with <code>ssh</code>).</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git remote add upstream <span class="highlight">https://www.github.com/username/repo-name</span>
</li></ul></code></pre>
<h2 id="staging">Staging</h2>

<p>When you’ve modified a file and have marked it to go in your next commit, it is considered to be a staged file.</p>

<p>Check the status of your Git repository, including files added that are not staged, and files that are staged. </p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git status
</li></ul></code></pre>
<p>To stage modified files, use the <code>add</code> command, which you can run multiple times before a commit. If you make subsequent changes that you want included in the next commit, you must run <code>add</code> again.</p>

<p>You can specify the specific file with <code>add</code>.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git add <span class="highlight">my_script.py</span>
</li></ul></code></pre>
<p>With <code>.</code> you can add all files in the current directory including files that begin with a <code>.</code>.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git add .
</li></ul></code></pre>
<p>You can remove a file from staging while retaining changes within your working directory with <code>reset</code>.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git reset <span class="highlight">my_script.py</span>
</li></ul></code></pre>
<h2 id="committing">Committing</h2>

<p>Once you have staged your updates, you are ready to commit them, which will record changes you have made to the repository.</p>

<p>To commit staged files, you’ll run the <code>commit</code> command with your meaningful commit message so that you can track commits. </p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git commit -m "Commit message"
</li></ul></code></pre>
<p>You can condense staging all tracked files with committing them in one step.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git commit -am "Commit message"
</li></ul></code></pre>
<p>If you need to modify your commit message, you can do so with the <code>--amend</code> flag.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git commit --amend -m "New commit message"
</li></ul></code></pre>
<h2 id="branches">Branches</h2>

<p>A branch in Git is a movable pointer to one of the commits in the repository, it allows you to isolate work and manage feature development and integrations. You can learn more about branches by reading the <a href="https://git-scm.com/book/en/v1/Git-Branching-What-a-Branch-Is">Git documentation</a>.</p>

<p>List all current branches with the <code>branch</code> command. An asterisk (<code>*</code>) will appear next to your currently active branch.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git branch
</li></ul></code></pre>
<p>Create a new branch. You will remain on your currently active branch until you switch to the new one.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git branch <span class="highlight">new-branch</span>
</li></ul></code></pre>
<p>Switch to any existing branch and check it out into your current working directory.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git checkout <span class="highlight">another-branch</span>
</li></ul></code></pre>
<p>You can consolidate the creation and checkout of a new branch by using the <code>-b</code> flag.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git checkout -b <span class="highlight">new-branch</span>
</li></ul></code></pre>
<p>Rename your branch name.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git branch -m <span class="highlight">current-branch-name</span> <span class="highlight">new-branch-name</span>
</li></ul></code></pre>
<p>Merge the specified branch’s history into the one you’re currently working in. </p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git merge <span class="highlight">branch-name</span>
</li></ul></code></pre>
<p>Abort the merge, in case there are conflicts. </p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git merge --abort
</li></ul></code></pre>
<p>You can also select a particular commit to merge with <code>cherry-pick</code> with the string that references the specific commit. </p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git cherry-pick <span class="highlight">f7649d0</span>
</li></ul></code></pre>
<p>When you have merged a branch and no longer need the branch, you can delete it.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git branch -d <span class="highlight">branch-name</span>
</li></ul></code></pre>
<p>If you have not merged a branch to master, but are sure you want to delete it, you can <strong>force</strong> delete a branch.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git branch -D <span class="highlight">branch-name</span>
</li></ul></code></pre>
<h2 id="collaborate-and-update">Collaborate and Update</h2>

<p>To download changes from another repository, such as the remote upstream, you’ll use <code>fetch</code>. </p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git fetch <span class="highlight">upstream</span>
</li></ul></code></pre>
<p>Merge the fetched commits.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git merge upstream/master
</li></ul></code></pre>
<p>Push or transmit your local branch commits to the remote repository branch.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git push origin master
</li></ul></code></pre>
<p>Fetch and merge any commits from the tracking remote branch.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git pull
</li></ul></code></pre>
<h2 id="inspecting">Inspecting</h2>

<p>Display the commit history for the currently active branch.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git log
</li></ul></code></pre>
<p>Show the commits that changed a particular file. This follows the file regardless of file renaming.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git log --follow <span class="highlight">my_script.py</span>
</li></ul></code></pre>
<p>Show the commits that are on one branch and not on the other. This will show commits on <code><span class="highlight">a-branch</span></code> that are not on <code><span class="highlight">b-branch</span></code>.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git log <span class="highlight">a-branch</span>..<span class="highlight">b-branch</span>
</li></ul></code></pre>
<p>Look at reference logs (<code>reflog</code>) to see when the tips of branches and other references were last updated within the repository.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git reflog
</li></ul></code></pre>
<p>Show any object in Git via its commit string or hash in a more human-readable format.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git show <span class="highlight">de754f5</span>
</li></ul></code></pre>
<h2 id="show-changes">Show Changes</h2>

<p>The <code>git diff</code> command shows changes between commits, branches, and more. You can read more fully about it through the <a href="https://git-scm.com/docs/git-diff">Git documentation</a>. </p>

<p>Compare modified files that are on the staging area.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git diff --staged
</li></ul></code></pre>
<p>Display the diff of what is in <code><span class="highlight">a-branch</span></code> but is not in <code><span class="highlight">b-branch</span></code>.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git diff <span class="highlight">a-branch</span>..<span class="highlight">b-branch</span>
</li></ul></code></pre>
<p>Show the diff between two specific commits.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git diff <span class="highlight">61ce3e6</span>..<span class="highlight">e221d9c</span>
</li></ul></code></pre>
<h2 id="stashing">Stashing</h2>

<p>Sometimes you’ll find that you made changes to some code, but before you finish you have to begin working on something else. You’re not quite ready to commit the changes you have made so far, but you don’t want to lose your work. The <code>git stash</code> command will allow you to save your local modifications and revert back to the working directory that is in line with the most recent <code>HEAD</code> commit.</p>

<p>Stash your current work.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git stash
</li></ul></code></pre>
<p>See what you currently have stashed.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git stash list
</li></ul></code></pre>
<p>Your stashes will be named <code>stash@{0}</code>, <code>stash@{1}</code>, and so on.</p>

<p>Show information about a particular stash.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git stash show stash@{<span class="highlight">0</span>}
</li></ul></code></pre>
<p>To bring the files in a current stash out of the stash while still retaining the stash, use <code>apply</code>.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git stash apply stash@{<span class="highlight">0</span>}
</li></ul></code></pre>
<p>If you want to bring files out of a stash, and no longer need the stash, use <code>pop</code>.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git stash pop stash@{<span class="highlight">0</span>}
</li></ul></code></pre>
<p>If you no longer need the files saved in a particular stash, you can <code>drop</code> the stash.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git stash drop stash@{<span class="highlight">0</span>}
</li></ul></code></pre>
<p>If you have multiple stashes saved and no longer need to use any of them, you can use <code>clear</code> to remove them.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git stash clear
</li></ul></code></pre>
<h2 id="ignoring-files">Ignoring Files</h2>

<p>If you want to keep files in your local Git directory, but do not want to commit them to the project, you can add these files to your <code>.gitignore</code> file so that they do not cause conflicts.</p>

<p>Use a text editor such as nano to add files to the <code>.gitignore</code> file.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">nano .gitignore
</li></ul></code></pre>
<p>To see examples of <code>.gitignore</code> files, you can look at GitHub’s <a href="https://github.com/github/gitignore"><code>.gitignore</code> template repo</a>.</p>

<h2 id="rebasing">Rebasing</h2>

<p>A rebase allows us to move branches around by changing the commit that they are based on. With rebasing, you can squash or reword commits.</p>

<p>You can start a rebase by either calling the number of commits you have made that you want to rebase (<code>5</code> in the case below).</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git rebase -i HEAD~<span class="highlight">5</span>
</li></ul></code></pre>
<p>Alternatively, you can rebase based on a particular commit string or hash.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git rebase -i <span class="highlight">074a4e5</span>
</li></ul></code></pre>
<p>Once you have squashed or reworded commits, you can complete the rebase of your branch on top of the latest version of the project’s upstream code. </p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git rebase <span class="highlight">upstream/master</span>
</li></ul></code></pre>
<p>To learn more about rebasing and updating, you can read <a href="https://www.digitalocean.com/community/tutorials/how-to-rebase-and-update-a-pull-request">How To Rebase and Update a Pull Request</a>, which is also applicable to any type of commit.</p>

<h2 id="resetting">Resetting</h2>

<p>Sometimes, including after a rebase, you need to reset your working tree. You can reset to a particular commit, and <strong>delete all changes</strong>, with the following command.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git reset --hard <span class="highlight">1fc6665</span>
</li></ul></code></pre>
<p>To force push your last known non-conflicting commit to the origin repository, you’ll need to use <code>--force</code>. </p>

<p><span class='warning'><strong>Warning</strong>: Force pushing to master is often frowned upon unless there is a really important reason for doing it. Use sparingly when working on your own repositories, and work to avoid this when you’re collaborating.<br></span></p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git push --force origin master
</li></ul></code></pre>
<p>To remove local untracked files and subdirectories from the Git directory for a clean working branch, you can use <code>git clean</code>.</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git clean -f -d
</li></ul></code></pre>
<p>If you need to modify your local repository so that it looks like the current upstream master (that is, there are too many conflicts), you can perform a hard reset.</p>

<p><span class='note'><strong>Note</strong>: Performing this command will make your local repository look exactly like the upstream. Any commits you have made but that were not pulled into the upstream <strong>will be destroyed</strong>.<br></span></p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git reset --hard upstream/master
</li></ul></code></pre>
<h2 id="conclusion">Conclusion</h2>

<p>This guide covers some of the more common Git commands you may use when managing repositories and collaborating on software. </p>

<p>You can learn more about open-source software and collaboration in our <a href="https://www.digitalocean.com/community/tutorial_series/an-introduction-to-open-source">Introduction to Open Source tutorial series</a>:</p>

<ul>
<li><a href="https://www.digitalocean.com/community/tutorials/how-to-contribute-to-open-source-getting-started-with-git">How To Contribute to Open Source: Getting Started with Git</a></li>
<li><a href="https://www.digitalocean.com/community/tutorials/how-to-create-a-pull-request-on-github">How To Create a Pull Request on GitHub</a></li>
<li><a href="https://www.digitalocean.com/community/tutorials/how-to-rebase-and-update-a-pull-request">How To Rebase and Update a Pull Request</a></li>
<li><a href="https://www.digitalocean.com/community/tutorials/how-to-maintain-open-source-software-projects">How To Maintain Open-Source Software Projects</a></li>
</ul>

<p>There are many more commands and variations that you may find useful as part of your work with Git. To learn more about all of your available options, you can run:</p>
<pre class="code-pre command"><code langs=""><ul class="prefixed"><li class="line" prefix="$">git --help
</li></ul></code></pre>
<p>To receive useful information. You can also read more about Git and look at Git’s documentation from the <a href="https://git-scm.com/">official Git website</a>.</p>

    </div>
</div>

  </body>
</html>
