# Ada Programming Language
https://ada-lang.io/
Learning Materials - https://adaic.org/learn/materials/

# install Ada
$ doas apt-get install gnat

# compile Ada program
$ gnatmake program.adb
