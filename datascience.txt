Data Scientist

Job description: Your job will be to clean and explore datasets, and make predictions 
that deliver business value. Your day-to-day will involve training and optimizing 
models, and often deploying them to production.

Why it's important: When you have a pile of data that's too big for a human to prase, 
and too valuable to be ignored, you need some way of pulling digestible insights from
it. That's the basic job of a data scientist: to convert datasets into digestible 
conclusions.

Requirements: The technologies you'll be working with include Python, scikit-learn,
Pandas, SQL, and possibly Flask, Spark and/or TensorFlow/PyTorch. Some data science 
positions are purely technical, but the majority will require you to have some business 
sense, so that you don't end up solving problems that no one has.

data36- https://data36.com/python-syntex-essentials-and-best-practices/
kaagle.com


10 Best Data Science Education Platforms:

01. Best Data Science Education Platform For Beginners: Mode
02. Best Data Science Education Platform For Experienced Users: StrataScratch
03. Best Data Science Education Platform For a Specific Language: Udemy
04. Best Data Science Education Platform For a Specific Job: HackerRank
05. Best Free Data Science Education Platform: Exercism
06. Best Data Science Education Platform For Hands-on Learners: LeetCode
07. Best Data Science Education Platform For the Fundamentals: Qvault
08. Best Data Science Education Platform For Certification: Coursera
09. Best Self-Driven Data Science Education Platform: DataBricks Book
10. Best Lecture-Based Data Science Education Platform: Exponent
