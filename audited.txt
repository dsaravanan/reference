Default umask in /etc/login.defs changed from 022 to 027
Default umask in /etc/init.d/rc changed from 022 to 027

Enabled sysstat to collect accounting
vi /etc/default/sysstat

# line 9: change
ENABLED="true"

# systemctl restart sysstat
