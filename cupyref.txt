# cupy reference

lscpu

CUDA version:
nvcc --version

replace numpy and scipy with cupy and cupyx.scipy

Basics of CuPy
https://docs.cupy.dev/en/stable/user_guide/basic.html


NumPy/CuPy APIs Comparison Table
https://docs.cupy.dev/en/stable/reference/comparison.html

numpy.DataSource                cupy.DataSource (alias of numpy.DataSource)
numpy.ScalarType                -
numpy.abs                       cupy.abs
numpy.absolute                  cupy.absolute
numpy.add                       cupy.add
numpy.all                       cupy.all
numpy.allclose                  cupy.allclose
numpy.alltrue                   cupy.alltrue
numpy.amax                      cupy.amax
numpy.amin                      cupy.amin
numpy.angle                     cupy.angle
numpy.any                       cupy.any
numpy.append                    cupy.append
numpy.apply_along_axis          cupy.apply_along_axis
numpy.apply_over_axes           -
numpy.arange                    cupy.arange
numpy.arccos                    cupy.arccos
numpy.arccosh                   cupy.arccosh
numpy.arcsin                    cupy.arcsin
numpy.arcsinh                   cupy.arcsinh
