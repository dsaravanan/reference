Open data movement links

google public data - www.google.com/publicdata
World Band Data - http://data.worldbank.org/developers
U.S. Data - http://www.data.gov/
U.K. Data - http://data.gov.uk/

# huge collection of open data sets on a variety of subjects
Research Pipeline - http://www.researchpipeline.com/datasets.html

# potential of open data
http://www.ted.com/talks/tim_berners_lee_the_year_open_data_went_worldwide.html

RBI database of Indian economy - http://tiny-url.com/netspeak-rbi
Election Commission of India data - http://eci.nic.in/

# open civic - a movement to liberate data in non-machine friendly form into
# machine readable/re-mixable format - http://www.opencivic.in/

Akshay Surve - http://akshaysurve.com/

# on-line storage locations to find a wealth of digital resources
Rapidshare and Megaupload

# download tool (free software)
Mipony - http://www.mipony.net/
