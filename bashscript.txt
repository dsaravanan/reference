shebang:
#!/bin/bash
#!/usr/bin/env bash - highly prefered

Bash is basically a glue to the C programs.
Bash is case sensitive.

1. Variables

var1="good"
echo $var1

when you do variable assignment in bash no space around in eqaul sign.

var2 = "bad"
echo $var2

2. Command substitution

built-in bash commands assignment to a variable:
my_dir=$(pwd)
my_dir=`pwd`

3. Arrays

arr=(hello universe)
echo ${arr[0]}
echo ${arr[1]}

hello
universe

arr="hello universe"
echo ${arr[0]}
echo ${arr[1]}

hello universe

count number of elements in an array
arr=(hello universe)
echo ${#arr[*]}

2

4. Quoting
space is a delimiter in bash
in bash single quote is a literal string - i.e. prints exactly the string contents

echo "$(pwd)"
/home/user

echo '$(pwd)'
$pwd

5. Math
result=$((4 + 6))

echo $result
10

echo "$result"
10

6. Tests statements

null string
echo $([ -z "hi" ])
echo $([ -z "" ])
echo $([ ! -z "" )]

execute echo command only if [ -z "" ] is true (&& - and, || - or)
[ -z "" ] && echo "hi"

execute both commands
[ -z "" ]; echo "hi"

7. if statements

if the string is null then echo "hi"

if [ -z "hi" ]; then echo "hi"
fi

if [ -z "hi" ]
then
	echo "hi"
fi

if [ -z "" ]; then echo "hi"
fi

if [ -z "" ] && [ 0=0 ]; then echo "hi"
fi

if [ -z "" ] || [ 0=0 ]; then echo "hi"
fi

don't do these: (not posix)
[[ test1 && test2 ]]
[[ test1 || test2 ]]

8. case statements

filetype=".md"
filetype=".tex"
filetype=".rmd"
filetype=".Rmd"
filetype=".sh"

case $filetype in
	*.md) echo "You chose markdown" ;;
	*.tex) echo "You chose a TeX file! | sed 's/!/, and LaTeX is best/g' ;;
	*.[rR]md) echo "You chose R Markdown" ;; # regex statement
	*) echo "You chose nothing, this is the default" ;;
esac

sed 's/!/, and LaTeX is best/g' # (s-substitute) ! (with) , and LaTex is best (g-globally)

9. Loops

-le = less then equal to
-ge = greater then equal to
-lt = lesser then
-gt = greater then

while loops

counter=1

while [ $counter -le 10 ]
do
	echo $counter
	((counter++))
done

until loops

counter=1

until [ $counter -ge 10 ]
do
	echo $counter
	((counter++))
done

for loop

names='raman lakshman'

for name in $names
do
	echo $name
done

10. Braces expansion and ranges

touch {file1,file2,file3}.txt

touch file{1..10}.txt
mkdir Directory{1..10}
rm file{1..5}.txt
rmdir Directory{1..5}

for value in {1..5}
do
	echo $value
done

11. Globbing through wild cards
rm file*.txt
rm file{1,3,5}.txt

distrotube did a great video on globbing

12. regex - regular expressions
^ - at the beginning of the line

grep "^\- \[ \]*" ~/notes.txt

13. Functions

function myFunc() {
	echo "this is bad practice"
}

myFunc() {
	echo "this is good practice"
}

myFunc

14. shellcheck

Compare strings in Bash:

string1 = string2 	# use the = operator with the test [ command
string1 == string2 	# use the == operator with the [[ command for pattern matching
string1 != string2 	# inequality operator returns true if the operands are not equal
string1 =~ regex 	# regex operator returns true if the left operand matches the
					  extended regular expression on the right
string1 > string2 	# greater than operator returns true if the left operand is greater
					  than the right sorted by lexicographical order
string1 < string2 	# less than operator returns true if the right operand is greater than
					  the right sorted by lexicographical order
-z string			# true if the string length is zero
-n string			# true if the string length is non-zero

echo * (alternative to ls command)
echo *.jpg

# print file size
du -sh $(ls -l | tr -s ' ' | cut -d ' ' -f 9) | less
